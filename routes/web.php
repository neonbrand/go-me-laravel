<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    Auth::logout();
    abort(403);
});

Route::get('/login', function() {
    Auth::logout();
    abort(404);
});

Route::post('/login', function() {
    Auth::logout();
    abort(404);
});

Route::post('/logout', function() {
    Auth::logout();
    abort(404);
});

Auth::routes(['register' => false]);

Route::get('/home', function() {
    return view('home');
});