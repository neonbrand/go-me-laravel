<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Auth\ApiAuthController@register');
Route::post('/login', 'Auth\ApiAuthController@login');
Route::post('/login/facebook', 'Auth\ApiAuthController@loginFacebook');
Route::post('/refresh', 'Auth\ApiAuthController@refresh');

// Route::get('login/facebook/redirect', 'Auth\LoginController@redirectToProvider');
// Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
//
// Route::get('login/google/redirect', 'Auth\LoginController@redirectToProvider');
// Route::get('/google/callback', 'Auth\LoginController@handleProviderCallback');

Route::prefix('v1')->group(function() {
    Route::middleware('auth:api')->group(function () {
        Route::post('/logout', 'Auth\ApiAuthController@logout');
        Route::resource('/user', 'UserController');
        Route::resource('/workout', 'WorkoutController');
        Route::get('/goal/type/{type}', 'GoalController@scopedGoals');
        Route::get('/goal/type/{type}/workouts', 'GoalController@scopedGoalsWithWorkouts');
        Route::resource('/goal', 'GoalController');
        // Route::resource('/goal', 'GoalController');
    });
});