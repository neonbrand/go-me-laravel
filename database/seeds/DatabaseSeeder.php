<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        factory(\App\Goal::class)->create([
                                              'name'         => 'Boston Marathon',
                                              'type'         => 'Running',
                                              'banner_image' => asset('banner/running-boston-marathon-small.png'),
                                              'hero_image'   => asset('hero/running-boston-marathon-large.png'),
                                              'distance' => 26,
                                              'description' => "Run the famed Boston Marathon! The route begins in Hopkinton, flows through the hilly terrain of eastern Massachusetts, and ends on historical Boylston Street in Boston.",
                                              'difficulty' => 3,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'Grand Canyon Rim to Rim',
                                              'type'         => 'Running',
                                              'banner_image' => asset('banner/running-grand-canyon-small.png'),
                                              'hero_image'   => asset('hero/running-grand-canyon-large.png'),
                                              'distance' => 23.9,
                                              'description' => "This grand run is no walk in the park. Starting on the edge of the canyon, you’ll run down the canyon and come back up to finish on the other side. Be sure to take in the view along the way!",
                                              'difficulty' => 2,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'Skyline to Sea trail run',
                                              'type'         => 'Running',
                                              'banner_image' => asset('banner/running-skyline-to-sea-small.png'),
                                              'hero_image'   => asset('hero/running-skyline-to-sea-large.png'),
                                              'distance' => 31,
                                              'description' => "Take on one of the most popular hiking routes in California. This hike takes you across the Santa Cruz Mountains, through redwood trees, and ends at sandy Waddell Beach.",
                                              'difficulty' => 4,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'Lake Michigan',
                                              'type'         => 'Rowing',
                                              'banner_image' => asset('banner/rowing-lake-michigan-small.png'),
                                              'hero_image'   => asset('hero/rowing-lake-michigan-large.png'),
                                              'distance' => 190,
                                              'description' => "Row across one of the largest lakes in the world! When you’re in the middle of crossing this massive body, you’ll feel like you’re in the middle of the ocean.",
                                              'difficulty' => 5,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'San Francisco Bay',
                                              'type'         => 'Rowing',
                                              'banner_image' => asset('banner/rowing-san-fran-small.png'),
                                              'hero_image'   => asset('hero/rowing-san-fran-large.png'),
                                              'distance' => 97,
                                              'description' => "It’s the Battle of the Bay! This row will take you from the top of the San Francisco Bay to the bottom. Be sure to look up as you row under the famous Golden Gate Bridge.",
                                              'difficulty' => 4,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'Fort Lauderdale to Miami',
                                              'type'         => 'Rowing',
                                              'banner_image' => asset('banner/rowing-miami-small.png'),
                                              'hero_image'   => asset('hero/rowing-miami-large.png'),
                                              'distance' => 46,
                                              'description' => "Ever wanted to row through the Atlantic Ocean? Take this path along the coast of Florida, where you’ll start in Fort Lauderdale and end your ride with a party in magical Miami.",
                                              'difficulty' => 3,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'Moab Century Tour',
                                              'type'         => 'Biking',
                                              'banner_image' => asset('banner/cycling-moab-utah-small.png'),
                                              'hero_image'   => asset('hero/cycling-moab-utah-large.png'),
                                              'distance' => 60,
                                              'description' => "Take a trip past the most famous arch in the world! Ride past the familiar Arches National Park, climb up above Moab, and finish at the epic Dead Horse State Park.",
                                              'difficulty' => 2,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'Great Allegheny Passage',
                                              'type'         => 'Biking',
                                              'banner_image' => asset('banner/cycling-great-allegheny-passage-small.png'),
                                              'hero_image'   => asset('hero/cycling-great-allegheny-passage-large.png'),
                                              'distance' => 150,
                                              'description' => "This 150 miler takes you from Washington, D.C. to Pittsburgh—through mountains, over valleys, alongside three rivers, and across the Mason-Dixon Line.",
                                              'difficulty' => 5,
                                          ]);
        factory(\App\Goal::class)->create([
                                              'name'         => 'Tahoe Sierra Century',
                                              'type'         => 'Biking',
                                              'banner_image' => asset('banner/cycling-tahoe-small.png'),
                                              'hero_image'   => asset('hero/cycling-tahoe-large.png'),
                                              'distance' => 100,
                                              'description' => "A century ride through some of the best scenery in the country. The peak of the ride is about halfway through when you’ll ride along Tahoe’s shoreline for 13 miles.",
                                              'difficulty' => 4,
                                          ]);

        factory(\App\User::class, 10)->create();

        factory(\App\Workout::class, 1000)->create();
    }
}
