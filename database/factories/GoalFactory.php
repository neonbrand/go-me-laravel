<?php

use App\Goal;
use Faker\Generator as Faker;

$continents = [
    'Africa',
    'Antarctica',
    'Australia',
    'Asia',
    'Europe',
    'North America',
    'South America',
];

$factory->define(Goal::class, function (Faker $faker) use($continents) {
    return [
        'name' => title_case($faker->words(2, true)) . ' ' . array_random(['Trail', 'Pilgrimage', 'Way', 'Walk']),
        'type' => array_random(['Biking', 'Walking', 'Rowing']),
        'distance' => rand(10,1000),
        'continent' => array_random($continents),
        'hero_image' => '',
        'difficulty' => rand(1,5),
        'description' => $faker->paragraphs(2, true),
        'banner_image' => '',
    ];
});
