<?php

use App\Goal;
use App\User;
use App\Workout;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Workout::class, function (Faker $faker) {
    $user = User::inRandomOrder()->first();
    $goal = Goal::inRandomOrder()->first();

    return [
        'user_id' => $user->id,
        'goal_id' => $goal->id,
        'type' => array_random(['Biking', 'Walking', 'Rowing']),
        'distance' => rand(1,2000) / 100,
        'duration' => rand(10,60),
        'calories_burned' => rand(10,1000),
        'date' => Carbon::now()->format('Y-m-d'),
    ];
});