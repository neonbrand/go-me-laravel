<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users',
            function( Blueprint $table ) {
                $table->unsignedSmallInteger('height')->after('email')->default(0);
                $table->unsignedSmallInteger('weight')->after('height')->default(0);
                $table->unsignedTinyInteger('activity')->after('weight')->default(0);
                $table->string('goal')->after('activity')->default("I want to motivate myself!");
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users',
            function( Blueprint $table ) {
                $table->dropColumn('height');
                $table->dropColumn('weight');
                $table->dropColumn('activity');
                $table->dropColumn('goal');
            });
    }
}
