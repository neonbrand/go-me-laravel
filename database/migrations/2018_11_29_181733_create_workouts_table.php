<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateWorkoutsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('workouts',
            function( Blueprint $table ) {
                $table->increments('id');
                $table->unsignedInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
                $table->unsignedInteger('goal_id');
                $table->foreign('goal_id')->references('id')->on('goals');
                $table->string('type');
                $table->unsignedDecimal('distance');
                $table->unsignedInteger('duration');
                $table->unsignedInteger('calories_burned')->nullable();
                $table->date('date');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('workouts');
    }
}