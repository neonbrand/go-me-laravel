<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateGoalsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('goals',
            function( Blueprint $table ) {
                $table->increments('id');
                $table->string('name');
                $table->text('description');
                $table->unsignedDecimal('distance');
                $table->string('type');
                $table->string('continent');
                $table->unsignedInteger('difficulty');
                $table->string('hero_image')->nullable();
                $table->string('banner_image')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('goals');
    }
}
