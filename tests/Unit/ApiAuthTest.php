<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiAuthTest extends TestCase {

    use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_user_can_register_an_account() {
        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('POST',
                                                '/api/register',
                                                [
                                                    'name'                  => $this->faker->name,
                                                    'username'              => $this->faker->safeEmail,
                                                    'password'              => 'secret',
                                                    'password_confirmation' => 'secret',
                                                ]);

        // dump($response->decodeResponseJson());

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                                      'token' => [
                                          'token_type',
                                          'expires_in',
                                          'access_token',
                                          'refresh_token',
                                      ],
                                      'user'  => [
                                          'name',
                                          'email',
                                          'avatar',
                                      ]
                                  ]);
    }

    public function test_user_can_login_with_standard_auth() {
        $user = User::first();

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('POST',
                                                '/api/login',
                                                [
                                                    'username' => $user->email,
                                                    'password' => 'secret',
                                                ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                                      'token' => [
                                          'token_type',
                                          'expires_in',
                                          'access_token',
                                          'refresh_token',
                                      ],
                                      'user'  => [
                                          'name',
                                          'email',
                                          'avatar',
                                      ]
                                  ]);
    }

    public function test_login_facebook_user() {
        $testUserToken =
            "EAAE3aIaHde0BAFHdGoKIiJMWCFpw3mE2qOgSIrFQQsoxkZCCYJz3ZAmrveElxIn7rXU8VWtsRqZBXMnt236zev2LL58HztvMadE3Xf5nzCmCokLxeYAhxwDLwZAHZBUskuNGURwa3wN9TixXchH1g2At2tV76ndB9KZCQY5OrZAKAWNVnlOK7T7TQuDga2gZB7ZA4oTZCEvVlSupFrhhuqE8lD30Sy5XeJxXchHDZC2N8whagZDZD";

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('POST',
                                                '/api/login/facebook',
                                                [
                                                    'access_token' => $testUserToken,
                                                ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                                      'token' => [
                                          'token_type',
                                          'expires_in',
                                          'access_token',
                                          'refresh_token',
                                      ],
                                      'user'  => [
                                          'name',
                                          'email',
                                          'avatar',
                                      ]
                                  ]);
    }

    public function test_logout_revokes_access(  ) {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('POST',
                                                '/api/v1/logout');

        $response->assertStatus(204);
    }

    /**
     * Testing showing the password reset request page.
     */
    public function test_show_password_reset_request_page() {
        $this
            ->get(route('password.request'))
            ->assertSuccessful()
            ->assertSee('Reset Password')
            ->assertSee('E-Mail Address')
            ->assertSee('Send Password Reset Link');
    }

    /**
     * Testing submitting the password reset request with an invalid
     * email address.
     */
    public function test_submit_password_reset_request_invalid_email() {
        $this
            ->followingRedirects()
            ->from(route('password.request'))
            ->post(route('password.update'),
                   [
                       'email' => str_random(),
                   ])
            ->assertSuccessful()
            ->assertSee(__('validation.email',
                           [
                               'attribute' => 'email',
                           ]));
    }

    /**
     * Testing submitting the password reset request with an email
     * address not in the database.
     */
    public function test_submit_password_reset_request_email_not_found() {
        $this
            ->followingRedirects()
            ->from(route('password.request'))
            ->post(route('password.email'),
                   [
                       'email' => $this->faker->unique()->safeEmail,
                   ])
            ->assertSuccessful()
            ->assertSee(e(__('passwords.user')));
    }

    /**
     * Testing showing the reset password page.
     */
    public function test_show_password_reset_page() {
        $user = factory(User::class)->create();

        $token = Password::broker()->createToken($user);

        $this
            ->get(route('password.reset',
                        [
                            'token' => $token,
                        ]))
            ->assertSuccessful()
            ->assertSee('Reset Password')
            ->assertSee('E-Mail Address')
            ->assertSee('Password')
            ->assertSee('Confirm Password');
    }

    /**
     * Testing submitting the password reset page with an invalid
     * email address.
     */
    public function test_submit_password_reset_invalid_email() {
        $user = factory(User::class)->create([
                                                 'password' => bcrypt('secret'),
                                             ]);

        $token = Password::broker()->createToken($user);

        $password = str_random();

        $this
            ->followingRedirects()
            ->from(route('password.reset',
                         [
                             'token' => $token,
                         ]))
            ->post(route('password.update'),
                   [
                       'token'                 => $token,
                       'email'                 => str_random(),
                       'password'              => $password,
                       'password_confirmation' => $password,
                   ])
            ->assertSuccessful()
            ->assertSee(__('validation.email',
                           [
                               'attribute' => 'email',
                           ]));

        $user->refresh();

        $this->assertFalse(Hash::check($password, $user->password));

        $this->assertTrue(Hash::check('secret', $user->password));
    }

    /**
     * Testing submitting the password reset page with an email
     * address not in the database.
     */
    public function test_submit_password_reset_email_not_found() {
        $user = factory(User::class)->create([
                                                 'password' => bcrypt('secret'),
                                             ]);

        $token = Password::broker()->createToken($user);

        $password = str_random();

        $this
            ->followingRedirects()
            ->from(route('password.reset',
                         [
                             'token' => $token,
                         ]))
            ->post(route('password.update'),
                   [
                       'token'                 => $token,
                       'email'                 => $this->faker->unique()->safeEmail,
                       'password'              => $password,
                       'password_confirmation' => $password,
                   ])
            ->assertSuccessful()
            ->assertSee(e(__('passwords.user')));

        $user->refresh();

        $this->assertFalse(Hash::check($password, $user->password));

        $this->assertTrue(Hash::check('secret',
                                      $user->password));
    }

    /**
     * Testing submitting the password reset page with a password
     * that doesn't match the password confirmation.
     */
    public function test_submit_password_reset_password_mismatch() {
        $user = factory(User::class)->create([
                                                 'password' => bcrypt('secret'),
                                             ]);

        $token = Password::broker()->createToken($user);

        $password              = str_random();
        $password_confirmation = str_random();

        $this
            ->followingRedirects()
            ->from(route('password.reset',
                         [
                             'token' => $token,
                         ]))
            ->post(route('password.update'),
                   [
                       'token'                 => $token,
                       'email'                 => $user->email,
                       'password'              => $password,
                       'password_confirmation' => $password_confirmation,
                   ])
            ->assertSuccessful()
            ->assertSee(__('validation.confirmed',
                           [
                               'attribute' => 'password',
                           ]));

        $user->refresh();

        $this->assertFalse(Hash::check($password, $user->password));

        $this->assertTrue(Hash::check('secret',
                                      $user->password));
    }

    /**
     * Testing submitting the password reset page with a password
     * that is not long enough.
     */
    public function test_submit_password_reset_password_too_short() {
        $user = factory(User::class)->create([
                                                 'password' => bcrypt('secret'),
                                             ]);

        $token = Password::broker()->createToken($user);

        $password = str_random(5);

        $this
            ->followingRedirects()
            ->from(route('password.reset',
                         [
                             'token' => $token,
                         ]))
            ->post(route('password.update'),
                   [
                       'token'                 => $token,
                       'email'                 => $user->email,
                       'password'              => $password,
                       'password_confirmation' => $password,
                   ])
            ->assertSuccessful()
            ->assertSee(__('validation.min.string',
                           [
                               'attribute' => 'password',
                               'min'       => 6,
                           ]));

        $user->refresh();

        $this->assertFalse(Hash::check($password, $user->password));

        $this->assertTrue(Hash::check('secret',
                                      $user->password));
    }

    /**
     * Testing submitting the password reset page.
     */
    public function test_submit_password_reset() {
        $user = factory(User::class)->create([
                                                 'password' => bcrypt('secret'),
                                             ]);

        $token = Password::broker()->createToken($user);

        $password = str_random();

        $this
            ->followingRedirects()
            ->from(route('password.reset',
                         [
                             'token' => $token,
                         ]))
            ->post(route('password.update'),
                   [
                       'token'                 => $token,
                       'email'                 => $user->email,
                       'password'              => $password,
                       'password_confirmation' => $password,
                   ])
            ->assertSuccessful()
            ->assertSee(__('passwords.reset'));

        $user->refresh();

        $this->assertFalse(Hash::check('secret',
                                       $user->password));

        $this->assertTrue(Hash::check($password, $user->password));
    }
}