<?php

namespace Tests\Feature;

use App\Goal;
use App\User;
use App\Workout;
use Carbon\Carbon;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WorkoutTest extends TestCase {

    public function test_a_user_creates_a_workout() {
        $user = factory(User::class)->create();
        $goal = factory(Goal::class)->create();

        Passport::actingAs($user);

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('POST',
                                                '/api/v1/workout',
                                                [
                                                    'goal_id'         => $goal->id,
                                                    'type'            => 'Rowing',
                                                    'distance'        => rand(1, 2000) / 100,
                                                    'duration'        => rand(5, 60),
                                                    'calories_burned' => rand(0, 400),
                                                    'date'            => Carbon::now()->format('Y-m-d'),
                                                ]);

        $response->assertStatus(200)
                 ->assertJsonStructure([
                                           'goal_id', 'user_id', 'type', 'duration', 'calories_burned', 'date', 'distance'
                                       ]);
    }

    public function test_a_user_updates_a_workout() {
        $user    = factory(User::class)->create();
        $workout = factory(Workout::class)->create([ 'user_id' => $user->id ]);
        $goal    = factory(Goal::class)->create();

        Passport::actingAs($user);

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('PUT',
                                                "/api/v1/workout/{$workout->id}",
                                                [
                                                    'goal_id'         => $goal->id,
                                                    'type'            => $workout->type,
                                                    'duration'        => rand(5, 60),
                                                    'calories_burned' => rand(0, 400),
                                                    'date'            => Carbon::now()->format('Y-m-d'),
                                                    'distance'        => rand(1, 2000) / 100,
                                                ]);

        $response->assertStatus(200)
                 ->assertJsonStructure([
                                           'goal_id', 'user_id', 'type', 'duration', 'calories_burned', 'date', 'distance'
                                       ]);
    }

    public function test_a_user_cannot_update_other_user_workout() {
        $user    = factory(User::class)->create();
        $workout = factory(Workout::class)->create([ 'user_id' => $user->id ]);
        $goal    = factory(Goal::class)->create();

        $otherUser = factory(User::class)->create();
        Passport::actingAs($otherUser);

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('PUT',
                                                "/api/v1/workout/{$workout->id}",
                                                [
                                                    'goal_id'         => $goal->id,
                                                    'type'            => $workout->type,
                                                    'duration'        => rand(5, 60),
                                                    'distance'        => rand(1, 2000) / 100,
                                                    'calories_burned' => rand(0, 400),
                                                    'date'            => Carbon::now()->format('Y-m-d'),
                                                ]);
        // $content = $response->decodeResponseJson();
        // dump($content);
        $response->assertStatus(400)
                 ->assertJson([
                                  'message' => "No query results for model [App\Workout]."
                              ]);
    }

    public function test_user_can_delete_their_own_workout() {
        $user    = factory(User::class)->create();
        $workout = factory(Workout::class)->create([ 'user_id' => $user->id ]);

        Passport::actingAs($user);

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('DELETE',
                                                "/api/v1/workout/{$workout->id}");

        $response
            ->assertStatus(200)
            ->assertJson([
                             'message' => "The requested resource has been deleted."
                         ]);
    }

    public function test_user_cannot_delete_other_user_workout() {
        $user    = factory(User::class)->create();
        $workout = factory(Workout::class)->create([ 'user_id' => $user->id ]);

        $otherUser = factory(User::class)->create();
        Passport::actingAs($otherUser);

        $response = $this->withHeaders([
                                           'Accepted' => 'application/json',
                                       ])->json('DELETE',
                                                "/api/v1/workout/{$workout->id}");

        $response
            ->assertStatus(400)
            ->assertJson([
                             'message' => "No query results for model [App\Workout]."
                         ]);
    }
}
