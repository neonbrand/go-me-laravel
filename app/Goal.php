<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goal extends Model {

    protected $guarded = [
        'id'
    ];

    protected $withCount = [
        'workouts'
    ];

    protected $appends = [
        'progress'
    ];

    public function workouts() {
        return $this->hasMany(Workout::class);
    }

    public function getProgressAttribute() {
        return $this->workouts()
                    ->where('user_id', auth()->user()->id)
                    ->select(DB::raw('COALESCE(SUM(distance),0) as distance_cumulative'))
                    ->addSelect(DB::raw('COALESCE(SUM(calories_burned),0) as calories_burned_cumulative'))
                    ->addSelect(DB::raw('COALESCE(SUM(duration),0) as duration_cumulative'))
                    ->addSelect(DB::raw('MIN(date) as date_started'))
                    ->get();
    }
}
