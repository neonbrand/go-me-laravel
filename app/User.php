<?php

namespace App;

use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Schedula\Laravel\PassportSocialite\User\UserSocialAccount;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia, UserSocialAccount {

    use HasApiTokens, Notifiable, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        ''
    ];

    protected $appends = [
        'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function social() {
        return $this->hasOne(SocialiteUser::class);
    }

    public function getAvatarAttribute() {
        $url = $this->getFirstMediaUrl('avatars');
        if($url === "") {
            return "";
        } else {
            return asset($url);
        }
    }

    public function workouts() {
        return $this->hasMany(Workout::class);
    }

    public function getGoalsAttribute() {
        return $this->workouts->groupBy('goal_id')->map(function( $row ) {
            $array[ 'calories_burned_cumulative' ] = $row->sum('calories_burned');
            $array[ 'distance_cumulative' ]        = $row->sum('distance');
            $array[ 'time_cumulative' ]            = $row->sum('duration');
            $array[ 'date_started' ]               = $row->min('date');

            return $array;
        });
    }

    /**
     * Get user from social provider and from provider's user's id
     *
     * @param string $provider Provider name as requested from oauth e.g. facebook
     * @param string $id       Id used by provider
     */
    public static function findForPassportSocialite( $provider, $id ) {
        $account = SocialiteUser::where('provider', $provider)->where('provider_id', $id)->first();
        if($account) {
            if($account->user) {
                return $account->user;
            }
        }
    }

}
