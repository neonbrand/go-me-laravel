<?php

namespace App\Http\Controllers;

use App\Workout;
use Illuminate\Http\Request;

class WorkoutController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $user     = auth()->user();
            $workouts = $user->workouts;
            if(!$workouts->isEmpty()) {
                return response()->json($workouts, 200);
            } else {
                return response()->json('You have no workouts. Create one!', 200);
            }
        } catch( \Exception $e ) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Add a new resource to storage
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( Request $request ) {
        $request->validate([
                               "date"            => "required|date_format:Y-m-d",
                               "type"            => "required|string",
                               "goal_id"         => "required|exists:goals,id",
                               "distance"        => "required|numeric",
                               "calories_burned" => "integer",
                               "duration"        => "required|integer",
                           ]);

        try {
            $user                 = auth()->user();
            $request[ 'user_id' ] = $user->id;

            $workout = Workout::create(
                $request->all()
            );

            return response()->json($workout, 200);
        } catch( \Exception $e ) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {
        $request->validate([
                               "date"            => "required|date_format:Y-m-d",
                               "type"            => "required|string",
                               "goal_id"         => "required|exists:goals,id",
                               "distance"        => "required|numeric",
                               "calories_burned" => "integer",
                               "duration"        => "required|integer",
                           ]);

        try {
            $user                 = auth()->user();
            $request[ 'user_id' ] = $user->id;

            $workout = Workout::where('user_id', $user->id)->where('id', $id)->firstOrFail();

            $workout->update([
                                 $request->all()
                             ]);

            return response()->json($workout, 200);
        } catch( \Exception $e ) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        try {
            $user = auth()->user();

            $workout = Workout::where('user_id', $user->id)->where('id', $id)->firstOrFail();

            if($workout instanceof Workout) {
                $workout->delete();
            }

            return response()->json([
                                        'message' => "The requested resource has been deleted."
                                    ],
                                    200);
        } catch( \Exception $e ) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
