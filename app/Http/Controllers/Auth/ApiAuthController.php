<?php

namespace App\Http\Controllers\Auth;

use App\SocialiteUser;
use App\User;
use Laravel\Passport\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class ApiAuthController extends Controller {

    public function login( Request $request ) {
        $validated = Validator::make($request->all(),
                                     [
                                         'username' => 'required|max:255',
                                         'password' => 'required|min:6',
                                     ]);
        if(!$validated->fails()
           && Auth::attempt([
                                'email'    => $request[ 'username' ],
                                'password' => $request[ 'password' ]
                            ])
        ) {
            $user = Auth::user();

            return $this->proxyRequest($request, $user);

        } else {
            return response()->json([ 'error' => 'Unauthorized' ], 401);
        }
    }

    public function register( Request $request ) {
        $validated = Validator::make($request->all(),
                                     [
                                         'name'     => 'required|string|max:255',
                                         'username' => 'required|string|email|max:255|unique:users,email',
                                         'password' => 'required|string|min:6|confirmed',
                                     ]);

        if(!$validated->fails()) {
            $user = User::create([
                                     'name'     => $request[ 'name' ],
                                     'email'    => $request[ 'username' ],
                                     'password' => bcrypt($request[ 'password' ]),
                                 ]);

            return $this->proxyRequest($request, $user);

        } else {

            return response()->json($validated->errors()->all(), 400);
        }
    }

    public function logout() {
        try {
            auth()->user()->token()->revoke();

            return response()->json([ 'message' => "Your token is no longer valid." ], 204);
        } catch( \Exception $e ) {

            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }

    public function refresh( Request $request ) {
        $validated = Validator::make($request->all(),
                                     [
                                         'refresh_token' => 'required',
                                     ]);

        if(!$validated->fails()) {
            $client = Client::where('password_client', 1)->first();
            $request->request->add([
                                       'refresh_token' => $request[ 'refresh_token' ],
                                       'grant_type'    => 'refresh_token',
                                       'client_id'     => $client->id,
                                       'client_secret' => $client->secret,
                                       'scope'         => null,
                                   ]);

            $proxy = Request::create('/oauth/token', 'POST');

            $response = Route::dispatch($proxy);

            if($response->status() != 200) {
                return $response;
            }

            $token = json_decode($response->getContent());

            return response()->json([
                                        'token' => $token,
                                    ]);
        } else {

            return response()->json($validated->errors()->all(), 400);
        }
    }

    public function loginFacebook(Request $request) {
        try {

            $facebook = Socialite::driver('facebook')->userFromToken($request->access_token);
            if(!$exist =
                SocialiteUser::where('provider', 'facebook')->where('provider_id', $facebook->getId())->first()
            ) {

                User::where('email', $facebook->email)
                    ->firstOrCreate([ 'email' => $facebook->email ],
                                    [ 'name' => $facebook->name ]);

                $user = User::firstOrCreate([ 'email' => $facebook->email ],
                                            [ 'name'  => $facebook->name   ]);

                $user->clearMediaCollection('avatars');
                $largeUrl = str_replace('normal', 'large', $facebook->avatar);
                $user->addMediaFromUrl($largeUrl)->toMediaCollection('avatars');

                $socialiteUser = SocialiteUser::create([
                                                           'user_id'     => $user->id,
                                                           'provider'    => 'facebook',
                                                           'provider_id' => $facebook->id,
                                                       ]);
            }

            if(!isset($user)) {
                $user = SocialiteUser::where('provider_id', $facebook->getId())->first()->user;
            }

            return response()->json(
                [
                    'token' => $this->issueToken($request, 'facebook', $request->access_token),
                    'user'  => $user->toArray(),
                ]
            );
        } catch( \Exception $e ) {
            return response()->json([ "error" => $e->getMessage() ]);
        }

    }

    public function issueToken($request, $provider, $accessToken) {
        /**
         * Here we will request our app to generate access token
         * and refresh token for the user using its social identity by providing access token
         * and provider name of the provider. (I hope its not confusing)
         * and then it goes through social grant and which fetches providers user id then calls
         * findForPassportSocialite from your user model if it returns User object then it generates
         * oauth tokens or else will throw error message normally like other oauth requests.
         */
        $client = Client::where('password_client', 1)->first();

        $params = [
            'grant_type' => 'social',
            'client_id' => $client->id, // it should be password grant client
            'client_secret' => $client->secret,
            'accessToken' => $accessToken, // access token from provider
            'provider' => $provider, // i.e. facebook
        ];
        $request->request->add($params);

        $requestToken = Request::create("oauth/token", "POST");
        $response = Route::dispatch($requestToken);

        return json_decode((string) $response->getContent(), true);
    }

    private function proxyRequest($request, $user) {
        $client = Client::where('password_client', 1)->first();

        $request->request->add([
                                   'username'      => $request[ 'username' ],
                                   'password'      => $request[ 'password' ],
                                   'grant_type'    => 'password',
                                   'client_id'     => $client->id,
                                   'client_secret' => $client->secret,
                                   'scope'         => null,
                               ]);

        $proxy = Request::create('/oauth/token', 'POST');

        $response = Route::dispatch($proxy);

        $token = json_decode($response->getContent());

        return response()->json([
                                    'token' => $token,
                                    'user'  => $user->toArray()
                                ]);
    }
}