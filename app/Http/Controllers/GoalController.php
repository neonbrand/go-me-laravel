<?php

namespace App\Http\Controllers;

use App\Goal;
use Illuminate\Http\Request;

class GoalController extends Controller {

    /*
     * Display scoped list of resource by the type
     *
     * @return \Illuminate\Http\Response
     */
    public function scopedGoals( $type ) {
        try {
            $user  = auth()->user();
            $goals = Goal::withCount([
                                         'workouts' => function( $q ) use ( $user ) {
                                             $q->where('user_id', $user->id);
                                         } ])
                         ->where('type', $type)
                         ->get();
            if(!$goals->isEmpty()) {
                return response()->json($goals, 200);
            } else {
                return response()->json('You have no goals. Create one!', 200);
            }
        } catch( \Exception $e ) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }

    /*
     * Display scoped list of resource by the type
     *
     * @return \Illuminate\Http\Response
     */
    public function scopedGoalsWithWorkouts( $type ) {
        try {
            $user  = auth()->user();
            $goals = Goal::whereHas('workouts',
                function( $q ) use ( $user ) {
                    $q->where('user_id', $user->id);
                })
                         ->withCount([
                                         'workouts' => function( $q ) use ( $user ) {
                                             $q->where('user_id', $user->id);
                                         } ])
                         ->with([
                                    'workouts' => function( $q ) use ( $user ) {
                                        $q->where('user_id', $user->id);
                                    } ])
                         ->where('type', $type)
                         ->get();
            if(!$goals->isEmpty()) {
                return response()->json($goals, 200);
            } else {
                return response()->json('You have no goals. Create one!', 200);
            }
        } catch( \Exception $e ) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $user  = auth()->user();
            $goals = Goal::whereHas('workouts',
                function( $q ) use ( $user ) {
                    $q->where('user_id', $user->id);
                })
                         ->withCount([
                                         'workouts' => function( $q ) use ( $user ) {
                                             $q->where('user_id', $user->id);
                                         } ])
                         ->with([
                                    'workouts' => function( $q ) use ( $user ) {
                                        $q->where('user_id', $user->id);
                                    } ])
                         ->get();
            if(!$goals->isEmpty()) {
                return response()->json($goals, 200);
            } else {
                return response()->json('You have no goals. Create one!', 200);
            }
        } catch( \Exception $e ) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }

    /**
     * Add a new resource to storage
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( Request $request ) {
        $request->validate([
                               "name"        => "required|string|max:255",
                               "description" => "required|string|max:65000",
                               "type"        => "required|string|max:255",
                               "continent"   => "required|string|max:255",
                               "difficulty"  => "required|integer|max:10",
                           ]);

        try {
            $user                 = auth()->user();
            $request[ 'user_id' ] = $user->id;

            $goal = Goal::create(
                $request->all()
            );

            return response()->json($goal, 200);
        } catch( \Exception $e ) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {
        $request->validate([
                               "name"        => "required|string|max:255",
                               "description" => "required|string|max:65000",
                               "type"        => "required|string|max:255",
                               "continent"   => "required|string|max:255",
                               "difficulty"  => "required|integer|max:10",
                           ]);

        try {
            $user                 = auth()->user();
            $request[ 'user_id' ] = $user->id;

            $goal = Goal::where('user_id', $user->id)->where('id', $id)->firstOrFail();

            $goal->update([
                              $request->all()
                          ]);

            return response()->json($goal, 200);
        } catch( \Exception $e ) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        try {
            $user = auth()->user();

            $goal = Goal::where('user_id', $user->id)->where('id', $id)->firstOrFail();

            if($goal instanceof Goal) {
                $goal->delete();
            }

            return response()->json([
                                        'message' => "The requested resource has been deleted."
                                    ],
                                    200);
        } catch( \Exception $e ) {
            return response()->json([ 'message' => $e->getMessage() ], 400);
        }
    }
}
