<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller {

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        return User::findOrFail($id)->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update( Request $request ) {
        try {
            $user = auth()->user();

            $request->validate([
                                   'username' => "email|unique:users,email,{$user->id}",
                                   'height'   => 'integer|max:65535',
                                   'weight'   => 'integer|max:65535',
                                   'activity' => 'integer',
                                   'goal'     => 'string|nullable',
                                   // TODO validate the avatar image
                                   // 'avatar' => 'mimes:jpeg',
                               ]);

            if($request->avatar != null) {
                $user->clearMediaCollection('avatars');
                $user->addMediaFromBase64($request->avatar)->toMediaCollection('avatars');
            }
            $user->update($request->except('avatar'));
            $updatedUser         = $user->fresh();
            $updatedUser->avatar = asset($user->getFirstMediaUrl('avatars'));

            return response()->json([ 'user' => $updatedUser ], 200);
        } catch(\Exception $e) {
            return response()->json('There was an error processing your request.', 400);
        }

    }
}
